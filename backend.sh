array=($(ccontrol qlist | tr "^" "\n"))

ls "${array[1]}/backend-java"

echo "Digite a namespace"
read NAMESPACE

FILE="${array[1]}/backend-java/${NAMESPACE}/shift-lis-api/config/application.properties"


[ -f $FILE ] && echo "Encontrei ${FILE}" || exit 1

echo "#### Parâmetros atuais:"
cat ${FILE} | grep 'quarkus.datasource.jdbc.initial-size=\|quarkus.datasource.jdbc.min-size\|quarkus.datasource.jdbc.max-size\|quarkus.datasource.jdbc.max-lifetime'

echo "#######################"

cp $FILE "${FILE}.bkptec"

sed -i 's:^[ \t]*quarkus.datasource.jdbc.initial-size[ \t]*=\([ \t]*.*\)$:quarkus.datasource.jdbc.initial-size='1':' ${FILE}
sed -i 's:^[ \t]*quarkus.datasource.jdbc.max-lifetime[ \t]*=\([ \t]*.*\)$:quarkus.datasource.jdbc.max-lifetime='0s':' ${FILE}
sed -i 's:^[ \t]*quarkus.datasource.jdbc.min-size[ \t]*=\([ \t]*.*\)$:quarkus.datasource.jdbc.min-size='1':' ${FILE}

service backend-${NAMESPACE} restart

echo "#### Parâmetros modificados:"
cat ${FILE} | grep 'quarkus.datasource.jdbc.initial-size=\|quarkus.datasource.jdbc.min-size\|quarkus.datasource.jdbc.max-size\|quarkus.datasource.jdbc.max-lifetime'
echo "#######################"


